using globalAPI.Business.Security;
using globalAPI.Interfaces.Business.Security;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Authentication;
    using globalAPI.Interfaces.Business.Administration;
using globalAPI.Business.Administration;
using Microsoft.Extensions.DependencyInjection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using globalAPI.Interfaces.Business.Ticket;
using globalAPI.Business.Ticket;
using globalAPI.Interfaces.DATA.Ticket;
using globalAPI.DATA.Ticket;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.


builder.Configuration.AddJsonFile("appsettings.json");
var secretKey = builder.Configuration["secretkey"];
var keyBytes = Encoding.UTF8.GetBytes(secretKey);

builder.Services.AddAuthentication(config =>
{
    config.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    config.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(config =>
{
    config.RequireHttpsMetadata = false;
    config.SaveToken = true;
    config.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(keyBytes),
        ValidateIssuer = false,
        ValidateAudience = false
    };
    
});
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors(options => {
    options.AddPolicy("nuevaPolitica", app =>
    {
        app.AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod();
    });
});

builder.Services.AddSingleton<IJwt, Jwt>();
builder.Services.AddSingleton<IUser, User>();
builder.Services.AddSingleton<globalAPI.Interfaces.DATA.IAdministration.IUser, globalAPI.DATA.Administration.User>();

builder.Services.AddSingleton<globalAPI.Interfaces.Business.Administration.IAuthentication, globalAPI.Business.Administration.Authentication>();
builder.Services.AddSingleton<ITicket, Ticket>();
builder.Services.AddSingleton<ITicketD, TicketD>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.UseAuthentication();
app.UseCors("nuevaPolitica");
app.UseAuthorization();

app.MapControllers();

app.Run();
