﻿

using globalAPI.Interfaces.Business.Security;
using globalAPI.MethodPatameters.Security;
using Newtonsoft.Json;

namespace ServicesGlobal.Security
{
    public class Security
    {
        private readonly IJwt jwt;

        public Security(IJwt jwt)
        {
            this.jwt = jwt;
        }

        public ValidateAdministrationTokenOut ValidateAdministrationToken(HttpRequest request, string actions)
        {
            var output = new ValidateAdministrationTokenOut() { result = globalAPI.Entities.General.Result.Error };
            string token = GetAuthorizationBearerToken(request);
            if (!string.IsNullOrEmpty(token))
            {
                output = jwt.ValidateAdministrationToken(new ValidateAdministrationTokenIn()
                {
                    token = token,
                    actions = actions
                });
            }
            return output;
        }

        private string GetAuthorizationBearerToken(HttpRequest request)
        {
            Microsoft.Extensions.Primitives.StringValues values;
            if (request.Headers.TryGetValue("Authorization", out values))
            {
                string token = values.FirstOrDefault();
                if (token.StartsWith("Bearer "))
                {
                    return token.Substring("Bearer ".Length);
                }
            }
            return null;
        }

    }
}
