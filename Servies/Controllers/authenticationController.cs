﻿using globalAPI.Interfaces.Business.Administration;
using globalAPI.Interfaces.Business.Security;
using globalAPI.MethodPatameters.Administration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Servies.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class authenticationController : ControllerBase
    {
        //private readonly IJwt jwt;
        private readonly globalAPI.Interfaces.Business.Administration.IAuthentication authentication;

        public authenticationController(globalAPI.Interfaces.Business.Administration.IAuthentication authentication)
        {
            //this.jwt = jwt;
            this.authentication = authentication;
        }

        [HttpPost]
        [Route("Login")]

        public async Task<IActionResult> Login(LoginIn input)
        {

            var output = await this.authentication.Login(input);

            return Ok(output);
        }
    }
}
