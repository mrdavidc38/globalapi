﻿using globalAPI.Interfaces.Business.Administration;
using globalAPI.Interfaces.Business.Security;
using globalAPI.MethodPatameters.Administration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ServicesGlobal.Security;

namespace Servies.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class userController : ControllerBase
    {
        private readonly globalAPI.Interfaces.Business.Administration.IUser user;
        

        public userController(globalAPI.Interfaces.Business.Administration.IUser user)
        {
            this.user = user;
          
        }
        [HttpPost]
        [Route("CreateUser")]
        public async Task<IActionResult> CreateUser(CreateUserIn input)
        {
            var output = new CreateUserOut();
             output = await this.user.CreateUser(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UpdateUserIn input)
        {
            var output = new UpdateUserOut();
            output = await this.user.UpdateUser(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("GetUser")]
        public async Task<IActionResult> GetUser(GetUserIn input)
        {
            var output = new GetUserOut();
            output = await this.user.GetUser(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("GetUserByID")]
        public async Task<IActionResult> GetUserByID(GetUserByIDIn input)
        {
            var output = new GetUserByIDOut();
            output = await this.user.GetUserByID(input);

            return Ok(output);
        }
    }
}
