﻿using globalAPI.Interfaces.Business.Ticket;
using globalAPI.MethodParameters.Ticket;
using globalAPI.MethodPatameters.Administration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Servies.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TicketsController : ControllerBase
    {
        private readonly ITicket _ticket;

        public TicketsController(ITicket ticket)
        {
            _ticket = ticket;

        }

        [HttpPost]
        [Route("CreateTicket")]
        public async Task<IActionResult> CreateTicket(createTicketsIn input)
        {
            var output = new createTicketsOut();
            output = await this._ticket.CreateTicket(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("GetAllTicket")]
        public async Task<IActionResult> GetAllTicket(getTicektsIn input)
        {
            var output = new getTicketsOut();
            output = await this._ticket.GetAllTicket(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("GetTicketByID")]
        public async Task<IActionResult> GetTicketByID(getTicektsIn input)
        {
            var output = new getTicketsOut();
            output = await this._ticket.GetTicketByID(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("UpdateTicket")]
        public async Task<IActionResult> UpdateTicket(updateTicketIn input)
        {
            var output = new updateTicketOut();
            output = await this._ticket.UpdateTicket(input);

            return Ok(output);
        }

        [HttpPost]
        [Route("DeleteTicket")]
        public async Task<IActionResult> DeleteTicket(deleteTicketIn input)
        {
            var output = new deleteTicketOut();
            output = await this._ticket.DeleteTicket(input);

            return Ok(output);
        }
    }
}
