﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters.Security
{
    public class ValidateAdministrationTokenIn
    {
        public string? token { get; set; }
        public string actions { get; set; }
    }
}
