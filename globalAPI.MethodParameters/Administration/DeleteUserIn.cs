﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters.Administration
{
    public class DeleteUserIn
    {
        public long userId { get; set; }
        public long userDeleteId
        {
            get; set;
        }
    }
}
