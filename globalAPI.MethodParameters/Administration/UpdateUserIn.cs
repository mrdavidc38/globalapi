﻿using globalAPI.Entities.onboarding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters.Administration
{
    public class UpdateUserIn
    {
        public User user { get; set; }
    }
}
