﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters.Administration
{
    public class LoginOut : BaseOut
    {
        public string token { get; set; }
        public string Name { get; set; }
        public string FirstSurname { get; set; }
        public string SecondSurname { get; set; }

        public long usuID { get; set; }

        public long proID { get; set; }
        public string usuPhone { get; set; }
        public string proName { get; set; }
  
    }
}
