﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters.Administration
{
    public class LoginIn
    {
        [Required]
        public int DocumentType { get; set; }

        [Required]
        public string usuName { get; set; }

        [Required]
        public string Password { get; set; }
        public string usuEmail { get; set; }
    }
}
