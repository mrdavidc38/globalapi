﻿using globalAPI.Entities.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.MethodPatameters
{
    public class BaseOut
    {
        public Result result { get; set; }
        public string message { get; set; }
    }
}
