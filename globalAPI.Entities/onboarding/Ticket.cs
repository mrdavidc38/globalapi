﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Entities.onboarding
{
    public class Ticket
    {
        public int? ticID { get; set; }
        public int? usuID { get; set; }
        public string? tic_description { get; set; }
        public string? tic_usuName { get; set; }
        public DateTime? tic_arrivalDate { get; set; }
        public DateTime? tic_departureDate { get; set; }
        public string? tic_status { get; set; }
        public DateTime? tic_creationDate { get; set; }
        public string? tic_userCreation { get; set; }
        public DateTime? tic_modDate { get; set; }
        public string? tic_userMod { get; set; }
    }
}
