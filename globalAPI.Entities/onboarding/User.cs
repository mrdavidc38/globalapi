﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Entities.onboarding
{
    public class User
    {
        public int usuID { get; set; }
        public string? usuName { get; set; }
        public string? usuEmail { get; set; }
        public string? usuStatus { get; set; }
  
        public int? usuUserCreation { get; set; }
        public string? usuPassword { get; set; }
        public int? usuUserMod { get; set; }
        public int? usuUserModDate { get; set; }
        public DateTime? usuCreationDate { get; set; }
        public int? proID { get; set; }  


    }
}
