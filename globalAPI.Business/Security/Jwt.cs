﻿using globalAPI.Interfaces.Business.Security;
using globalAPI.MethodPatameters.Security;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Business.Security
{
    public class Jwt : IJwt
    {
        private readonly IConfiguration Configuration;

        public Jwt(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        private string SecretAdministrationToken
        {
            get
            {
                return Environment.GetEnvironmentVariable("secretkey");
            }
        }


   

        private string KeyDecrypt
        {
            get
            {
                return Environment.GetEnvironmentVariable("key");
            }
        }

        public GenerateAdministrationTokenOut GenerateAdministrationToken(GenerateAdministrationTokenIn input)
        {
            var SecretAdministrationToken = Configuration["secretkey"];
        
            var output = new GenerateAdministrationTokenOut();
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretAdministrationToken);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[] {
                    new Claim("usuID", input.usuID.ToString()),
                    new Claim("proID",input.proID.ToString())
                    
                }),
                Expires = DateTime.UtcNow.AddHours(2),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            output.token = tokenHandler.WriteToken(token);
            return output;
        }

        public ValidateAdministrationTokenOut ValidateAdministrationToken(ValidateAdministrationTokenIn input)
        {
            var output = new ValidateAdministrationTokenOut() { result = Entities.General.Result.Error };
            if (string.IsNullOrEmpty(input.token))
            {
                return output;
            }

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretAdministrationToken);

            try
            {
                tokenHandler.ValidateToken(input.token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);
                var jwtToken = (JwtSecurityToken)validatedToken;
                output.usuID = long.Parse(jwtToken.Claims.First(x => x.Type == "usuID").Value);
                output.proID = long.Parse(jwtToken.Claims.First(x => x.Type == "proID").Value);

                output.result = Entities.General.Result.Success;
            }
            catch (Exception)
            {

                throw;
            }

            return output;  
        }

        public string DecryptData(string encryptedData)
        {
            var KeyDecrypt = Configuration["key"];
            
            var keybytes = Encoding.UTF8.GetBytes(KeyDecrypt);
            var iv = Encoding.UTF8.GetBytes(KeyDecrypt);

            var encrypted = Convert.FromBase64String(encryptedData);
            var decriptedFromJavascript = DecryptStringFromBytes(encrypted, keybytes, iv);

            return decriptedFromJavascript.ToString();
        }

        private string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
            {
                throw new ArgumentNullException("cipherText");
            }
            if (key == null || key.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }
            if (iv == null || iv.Length <= 0)
            {
                throw new ArgumentNullException("key");
            }

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an RijndaelManaged object
            // with the specified key and IV.
            using (var rijAlg = new RijndaelManaged())
            {
                //Settings
                rijAlg.Mode = CipherMode.CBC;
                rijAlg.Padding = PaddingMode.PKCS7;
                rijAlg.FeedbackSize = 128;

                rijAlg.Key = key;
                rijAlg.IV = iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = rijAlg.CreateDecryptor(rijAlg.Key, rijAlg.IV);

                try
                {
                    // Create the streams used for decryption.
                    using (var msDecrypt = new MemoryStream(cipherText))
                    {
                        using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                        {

                            using (var srDecrypt = new StreamReader(csDecrypt))
                            {
                                // Read the decrypted bytes from the decrypting stream
                                // and place them in a string.
                                plaintext = srDecrypt.ReadToEnd();

                            }

                        }
                    }
                }
                catch
                {
                    plaintext = "keyError";
                }
            }

            return plaintext;
        }




        public string EncryptStringAES(string plainText)
        {
            throw new NotImplementedException();
        }
    }
}
