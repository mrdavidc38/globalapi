﻿using globalAPI.Entities.onboarding;
using globalAPI.Interfaces.Business.Administration;
using globalAPI.Interfaces.DATA.IAdministration ;

using globalAPI.MethodPatameters.Administration;
using System;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Business.Administration
{
    public class User : globalAPI.Interfaces.Business.Administration.IUser
    {
        private readonly globalAPI.Interfaces.DATA.IAdministration.IUser _user;

        public User(globalAPI.Interfaces.DATA.IAdministration.IUser user)
        {
            _user = user;
        }

        public async Task<CreateUserOut> CreateUser(CreateUserIn input)
        {
            var output = new CreateUserOut() { result = Entities.General.Result.Error };

            var getUserOut = await _user.GetUser(new GetUserIn()
            {
                user = new globalAPI.Entities.onboarding.User()
                {
                    usuName = input.user.usuName,
                    usuEmail = input.user.usuEmail
                }
            });
            if (getUserOut.result == Entities.General.Result.NoRecords)
            {
                var createUserOut = await _user.CreateUser(new CreateUserIn()
                {
                    user = new globalAPI.Entities.onboarding.User()
                    {
                   
                        usuName = input.user.usuName,
                        usuEmail = input.user.usuEmail,
                        usuPassword = input.user.usuPassword,

                        usuStatus = input.user.usuStatus

                    }
                });

                if (createUserOut.result == Entities.General.Result.Success)
                {
                    output.result = Entities.General.Result.Success;
                }
            }
            else {
                output.result = Entities.General.Result.RecordAlreadyExists;
            }
            return output;
        }

        public async Task<GetUserOut> GetUser(GetUserIn input)
        {

            return await _user.GetUser(input);
        }

        public async Task<GetUserByIDOut> GetUserByID(GetUserByIDIn input)
        {
            return await _user.GetUserByID(input);
        }

        public string HashPassword(string input)
        {
            SHA256CryptoServiceProvider provider = new SHA256CryptoServiceProvider();
            byte[] inputBytes = Encoding.UTF8.GetBytes(input);
            byte[] hashedBytes = provider.ComputeHash(inputBytes);
            StringBuilder output = new StringBuilder();
            for (int i = 0; i < hashedBytes.Length; i++)
                output.Append(hashedBytes[i].ToString("x2").ToLower());
            return output.ToString();
        }

        public async Task<UpdateUserOut> UpdateUser(UpdateUserIn input)
        {
            var output = new UpdateUserOut();
            var updateUserout = await _user.UpdateUser(new UpdateUserIn()
            {
                user = new globalAPI.Entities.onboarding.User()
                {
                
                    usuName = input.user.usuName,
                    usuID = input.user.usuID,
                    usuEmail = input.user.usuEmail,
                    

                    usuStatus = input.user.usuStatus

                }
            });

            if (updateUserout.result == Entities.General.Result.Success)
            {
                output.result = Entities.General.Result.Success;
            }
            return output;
        }
    }
}
