﻿

using globalAPI.Business.Security;
using globalAPI.Interfaces.Business.Administration;
using globalAPI.Interfaces.Business.Security;
using globalAPI.MethodPatameters.Administration;
using globalAPI.MethodPatameters.Security;

namespace globalAPI.Business.Administration
{
    public class Authentication : IAuthentication
    {

        private readonly globalAPI.Interfaces.Business.Administration.IUser user;
        private readonly IJwt jwt;

        public Authentication(globalAPI.Interfaces.Business.Administration.IUser user, IJwt jwt)
        {
            this.user = user;
            this.jwt = jwt;
        }

        public async Task<LoginOut> Login(LoginIn input)
        {
            var output = new LoginOut();
            var getUserOut = await user.GetUser(new GetUserIn()
            {
                user = new globalAPI.Entities.onboarding.User()
                {
                    usuName = input.usuName,
                    usuEmail = input.usuEmail
                }
            });
            if (getUserOut.result == Entities.General.Result.Success)
            {
                if(getUserOut.user.usuStatus == "V")
                {
                    //string passDecrypt = jwt.DecryptData(input.Password);
                    var generateAdministrationTokenOut = new GenerateAdministrationTokenOut();
                    if (getUserOut.user.usuPassword.Equals(input.Password))
                    {
                      
                        generateAdministrationTokenOut = jwt.GenerateAdministrationToken(new GenerateAdministrationTokenIn()
                        {
                            usuID = getUserOut.user.usuID,
                            proID = 0
                        });
                    }

                    output.Name = getUserOut.user.usuName;

                    output.token = generateAdministrationTokenOut.token;
    
                    output.result = Entities.General.Result.Success;
                    output.usuID = getUserOut.user.usuID;

                    output.proID = 0;
            
   
                }
            }
            return output;
        }


    
        
    }
}
