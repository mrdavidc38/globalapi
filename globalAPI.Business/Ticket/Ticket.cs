﻿using globalAPI.Interfaces.Business.Ticket;
using globalAPI.Interfaces.DATA.Ticket;
using globalAPI.MethodParameters.Ticket;
using globalAPI.MethodPatameters.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Business.Ticket
{
    public class Ticket : ITicket
    {
        private readonly  ITicketD _ticekt;

        public Ticket(ITicketD ticekt)
        {
            _ticekt = ticekt;
        }

        public async Task<createTicketsOut> CreateTicket(createTicketsIn input)
        {
            return await _ticekt.CreateTicket(input);
        }

        public async Task<getTicketsOut> GetAllTicket(getTicektsIn input)
        {
            return await _ticekt.GetAllTicket(input);
        }

        public async Task<getTicketsOut> GetTicketByID(getTicektsIn input)
        {
            return await _ticekt.GetTicketByID(input);
        }

        public async Task<updateTicketOut> UpdateTicket(updateTicketIn input)
        {
            return await _ticekt.UpdateTicket(input);
        }

        public async Task<deleteTicketOut> DeleteTicket(deleteTicketIn input)
        {
            return await _ticekt.DeleteTicket(input);
        }
    }
}
