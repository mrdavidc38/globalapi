﻿using Dapper;
using globalAPI.Interfaces.DATA.Ticket;
using globalAPI.MethodParameters.Ticket;
using globalAPI.MethodPatameters.Administration;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.DATA.Ticket
{
    public class TicketD : ITicketD
    {
        private readonly IConfiguration Configuration;

        public TicketD(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public async Task<createTicketsOut> CreateTicket(createTicketsIn input)
        {
            var output = new createTicketsOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                var sp = "sp_CreateTickets"; ;
                parameters.Add("usuID", input.createTicket.usuID);
                parameters.Add("tic_description", input.createTicket.tic_description);
                parameters.Add("usuName", input.createTicket.tic_usuName);
                parameters.Add("tic_arrivalDate", input.createTicket.tic_arrivalDate);
                parameters.Add("tic_depureDate", input.createTicket.tic_departureDate);
                //parameters.Add("usuID", input.user.usuID);74

                try
                {
                    var result = await connection.QueryAsync<int>(sp, parameters, commandType: System.Data.CommandType.StoredProcedure);
                   

                    if (result.Any())
                    {
                        output.result = Entities.General.Result.Success;
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

          
            }
            return output;
        }

        public async Task<getTicketsOut> GetAllTicket(getTicektsIn input)
        {
            var output = new getTicketsOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                var sp = "sp_getTickets";

                try
                {
                    var result = await connection.QueryAsync<globalAPI.Entities.onboarding.Ticket>(sp, parameters, commandType: System.Data.CommandType.StoredProcedure);
                    var date = DateTime.Now;
                    output.getTicket = new List<globalAPI.Entities.onboarding.Ticket>();
                    if (result.Any())
                    {
                        foreach (var item in result.ToList())
                        {
                            var ticekt = new globalAPI.Entities.onboarding.Ticket()
                            {
                                ticID = item.ticID,
                                tic_description = item.tic_description == null ? "" : item.tic_description,
                                tic_usuName = item.tic_usuName == null ?"": item.tic_usuName,
                                tic_status = item.tic_status == null ?"": item.tic_status.Trim(),
                                tic_arrivalDate = item.tic_arrivalDate == null ? date : item.tic_arrivalDate,
                                tic_departureDate = item.tic_departureDate == null ? date : item.tic_departureDate,
                                usuID = item.usuID,


                            };

                            output.getTicket.Add(ticekt);

                        }
                        output.result = Entities.General.Result.Success;
                        //output.getTicket = result.ToList();
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

             
            }
            return output;
        }

        public async Task<getTicketsOut> GetTicketByID(getTicektsIn input)
        {
            var output = new getTicketsOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                var sp = "sp_getTickestByID";
                parameters.Add("usuID", input.getTicket.usuID);
                try
                {
                    var result = await connection.QueryAsync<globalAPI.Entities.onboarding.Ticket>(sp, parameters, commandType: System.Data.CommandType.StoredProcedure);
                    var date = DateTime.Now;
                    output.getTicket = new List<globalAPI.Entities.onboarding.Ticket>();
                    if (result.Any())
                    {
                        foreach (var item in result.ToList())
                        {
                            var ticekt = new globalAPI.Entities.onboarding.Ticket()
                            {
                                ticID = item.ticID,
                                tic_description = item.tic_description == null ? "" : item.tic_description,
                                tic_usuName = item.tic_usuName == null ? "" : item.tic_usuName,
                                tic_status = item.tic_status == null ? "" : item.tic_status.Trim(),
                                tic_arrivalDate = item.tic_arrivalDate == null ? date : item.tic_arrivalDate,
                                tic_departureDate = item.tic_departureDate == null ? date : item.tic_departureDate,
                                usuID = item.usuID,


                            };

                            output.getTicket.Add(ticekt);

                        }
                        output.result = Entities.General.Result.Success;
                        //output.getTicket = result.ToList();
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

            }
            return output;
        }

        public async Task<updateTicketOut> UpdateTicket(updateTicketIn input)
        {
            var output = new updateTicketOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                var sp = "sp_updateTicket"; ;
                parameters.Add("ticID", input.updateTicket.ticID);
                parameters.Add("tic_description", input.updateTicket.tic_description);
                parameters.Add("usuName", input.updateTicket.tic_usuName);
                parameters.Add("tic_arrivalDate", input.updateTicket.tic_arrivalDate);
                parameters.Add("tic_depureDate", input.updateTicket.tic_departureDate);
                parameters.Add("tic_status", input.updateTicket.tic_status);

                try
                {
                    var result = await connection.QueryAsync(sp, parameters, commandType: System.Data.CommandType.StoredProcedure);


                    if (result.Any())
                    {
                        output.result = Entities.General.Result.Success;
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
         
            }
            return output;
        }

        public async Task<deleteTicketOut> DeleteTicket(deleteTicketIn input)
        {
            var output = new deleteTicketOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                var sp = "sp_deleteTicket"; ;
                parameters.Add("ticID", input.usuID);
                //parameters.Add("usuID", input.user.usuID);74
                try
                {
                    var result = await connection.QueryAsync(sp, parameters, commandType: System.Data.CommandType.StoredProcedure);


                    if (result.Any())
                    {
                        output.result = Entities.General.Result.Success;
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }

        
            }
            return output;
        }
    }
}
