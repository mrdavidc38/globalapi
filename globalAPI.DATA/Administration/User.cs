﻿using Dapper;
using System.Data.SqlClient;
using globalAPI.MethodPatameters.Administration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using globalAPI.Interfaces.DATA.IAdministration;
using Microsoft.Extensions.Configuration;

namespace globalAPI.DATA.Administration
{
    public class User : IUser
    {
        private readonly IConfiguration Configuration;

        public User(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        private string OnboardingConnectionString
        {
            get
            {
                return Environment.GetEnvironmentVariable("OnboardingConnectionString");
            }
        }
        public async Task<CreateUserOut> CreateUser(CreateUserIn input)
        {
            var output = new CreateUserOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                //parameters.Add("proID", input.user.profile.proID);
                parameters.Add("usuName", input.user.usuName);
                parameters.Add("usuEmail", input.user.usuEmail);
                parameters.Add("usuPassword", input.user.usuPassword);
                //parameters.Add("usuID", input.user.usuID);74
               
                    output.userId = await connection.QuerySingleAsync<long>(@"INSERT INTO [User] (usuName,usuEmail,usuPassword, usuStatus,usuCreationDate, usuUserCreation ) VALUES (@usuName,@usuEmail,@usuPassword, 'V',GETDATE(), '' );SELECT CAST(SCOPE_IDENTITY() as int)", parameters);
                

                if (output.userId > 0)
                {
                    output.result = Entities.General.Result.Success;
                }
            }
            return output;
        }


        public async Task<GetUserByIDOut> GetUserByID(GetUserByIDIn input)
        {
            var output = new GetUserByIDOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];

            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                parameters.Add("usuID", input.usuID);
                try
                {
                    var result = await connection.QueryAsync<globalAPI.Entities.onboarding.User>(@"SELECT usuID,proID,usuName,usuEmail,usuPassword,usuStatus,usuUserCreation,usuCreationDate FROM dbo.[User] (NOLOCK) WHERE USUsTATUS <> 'E'", parameters);
                    if (result.Any())
                    {
                        output.User = result.ToList();
                        output.result = Entities.General.Result.Success;
                    }
                }
                catch (Exception ex)
                {

                    throw;
                }
            }
            return output;
        }


        public async Task<GetUserOut> GetUser(GetUserIn input)

        {
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];

            var output = new GetUserOut() { result = Entities.General.Result.Error };
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {

                string query = @"SELECT usuID,proID,usuName,usuEmail,usuPassword,usuStatus,usuUserCreation,usuCreationDate FROM dbo.[User] (NOLOCK) WHERE usuStatus<>'E'";
                connection.Open();
                var parameters = new DynamicParameters();

                if (input.user.usuID > 0)
                {
                    parameters.Add("usuID", input.user.usuID);
                    query = string.Concat(query, " AND usuID=@usuID");
                }
                if (!string.IsNullOrEmpty(input.user.usuName))
                {
                    parameters.Add("usuName", input.user.usuName);
                    query = string.Concat(query, " AND usuName=@usuName");
                }
                if (!string.IsNullOrEmpty(input.user.usuEmail) )
                {
                    parameters.Add("usuEmail", input.user.usuEmail);
                    query = string.Concat(query, " AND usuEmail=@usuEmail");
                }
            
                var userList = await connection.QueryAsync(query, parameters);
                if (userList.Any())
                {
                    var userTemp = userList.ToList().FirstOrDefault();
                    output.user = new globalAPI.Entities.onboarding.User()
                    {
                        usuID = userTemp.usuID,
            

                        usuName = userTemp.usuName,
                        usuPassword = userTemp.usuPassword,

                        usuStatus = userTemp.usuStatus,
                        usuUserCreation = 0,
                        usuCreationDate = userTemp.usuCreationDate,
                    };
                   
                    output.result = Entities.General.Result.Success;
                }
                else
                {
                    output.result = Entities.General.Result.NoRecords;
                }
            }
            return output;
        }


        public async Task<UpdateUserOut> UpdateUser(UpdateUserIn input)
        {
            var output = new UpdateUserOut() { result = Entities.General.Result.Error };
            var OnboardingConnectionString = Configuration["OnboardingConnectionString"];
            using (var connection = new SqlConnection(OnboardingConnectionString))
            {
                connection.Open();
                var parameters = new DynamicParameters();
                //parameters.Add("proID", input.user.profile.proID);
                parameters.Add("usuID", input.user.usuID);
                parameters.Add("usuName", input.user.usuName);
                parameters.Add("usuEmail", input.user.usuEmail);
                parameters.Add("usuStatus", input.user.usuStatus);
                
                parameters.Add("usuID", input.user.usuID);
                var result = await connection.QuerySingleAsync<long>(@"UPDATE [User] SET   usuName = @usuName, usuEmail=@usuEmail,usuStatus=@usuStatus WHERE usuID=@usuID;SELECT @@ROWCOUNT", parameters);
                if (result == 1)
                {
                    output.result = Entities.General.Result.Success;
                }
            }
            return output;
        }
    }
}
