#Bienvenido.
Saludo
#Description
Este Api fue creado en .Net 8, esta basado en una arquitectura "monolitica" de N capas: servicios, negocio, data y transversal, tambien tiene principios de inyección de dependencias. 

El Api expone tres controladores, uno encargado del CRUD para el módulo de usuarios, otro para la gestion de tickets y un tercero con un metodo de login encargado de generar el token que permite el paso a las distintas rutas y otorga acceso desde el Front.

Dentro de este repositorio hay un backUp de la BD construida en sql server para esta prueba en el cual hay una tabla de usuarios y una de ticket (tambien una de roles que pór tiempo no pude integrar al desarrollo), cada usuario tiene sus propios tickets que están sujetos al ID del user, si te logueas con el usuario de ID 9 por ejemplo, hay determinados tickets en el modulo.



