﻿using globalAPI.MethodParameters.Ticket;
using globalAPI.MethodPatameters.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Interfaces.Business.Ticket
{
    public interface ITicket
    {
        Task<createTicketsOut> CreateTicket(createTicketsIn input);
        Task<getTicketsOut> GetTicketByID(getTicektsIn input);
        Task<getTicketsOut> GetAllTicket(getTicektsIn input);

        Task<updateTicketOut> UpdateTicket(updateTicketIn input);

        Task<deleteTicketOut> DeleteTicket(deleteTicketIn input);
    }
}
