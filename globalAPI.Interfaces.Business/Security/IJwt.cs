﻿using globalAPI.MethodPatameters.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Interfaces.Business.Security
{
    public interface IJwt
    {

        ValidateAdministrationTokenOut ValidateAdministrationToken(ValidateAdministrationTokenIn input);

        GenerateAdministrationTokenOut GenerateAdministrationToken(GenerateAdministrationTokenIn input);


        string DecryptData(string encryptedData);

        string EncryptStringAES(string plainText);
    }
}
