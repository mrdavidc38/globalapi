﻿using globalAPI.MethodPatameters.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Interfaces.Business.Administration
{
    public interface IAuthentication
    {
        Task<LoginOut> Login(LoginIn input);
    }
}
