﻿using globalAPI.MethodPatameters.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace globalAPI.Interfaces.Business.Administration
{
    public  interface IUser
    {
        Task<CreateUserOut> CreateUser(CreateUserIn input);

        Task<GetUserOut> GetUser(GetUserIn input);

        Task<UpdateUserOut> UpdateUser(UpdateUserIn input);

        Task<GetUserByIDOut> GetUserByID(GetUserByIDIn input);

        string HashPassword(string input);
    }
}
